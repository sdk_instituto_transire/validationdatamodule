package com.example.validardadoscadastrais.ValidarDados.webService


import android.telecom.Call
import androidx.room.Query
import retrofit2.http.GET


interface CnpjEndPoint {

    @GET("cnpj")
    fun getCnpj(@retrofit2.http.Query("cnpj")cnpj: String, @retrofit2.http.Query("token")Token: String) : retrofit2.Call<Getcnpj>


}