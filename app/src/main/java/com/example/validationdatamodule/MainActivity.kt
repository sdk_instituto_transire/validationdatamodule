package com.example.validationdatamodule

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.validardadoscadastrais.ValidarDados.base.Mask
import com.example.validardadoscadastrais.ValidarDados.util.CpfUtil
import com.example.validationdatamodule.ValidarDados.util.CnpJUtil
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        insere_dados_CNPJ.addTextChangedListener(Mask.mask("##.###.###/####-##", insere_dados_CNPJ))
        botao_ler.setOnClickListener {
            result_dados_CNPJ.text = CnpJUtil.validationCNPJ(insere_dados_CNPJ.text.toString()).toString()
        }

        insere_dados_CPF.addTextChangedListener(Mask.mask("###.###.###-##", insere_dados_CPF))

        botao_ler_cpf.setOnClickListener {
            result_dados_CPF.text = CpfUtil.validationCPF(insere_dados_CPF.text.toString()).toString()
        }

    }
}